// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ConcertHallsTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CONCERTHALLSTEST_API AConcertHallsTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
