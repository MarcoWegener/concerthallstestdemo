# VR Listening Test Template



This project template provides a starting off point to create listening experiments in VR by including some useful plugins and project settings.
So far only compatible with UE 4.27.

## Content
- Controllable VRPawn
- VAReceiverActor to connect to [Virtual Acoustics](https://www.virtualacoustics.org/)
- Example Map with VA Sound

## Plugins
- [RWTH VR Toolkit](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/rwth-vr-toolkit)
- [Virtual Acoustics Plugin](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/unreal-va-plugin)
- [Universal Logging Plugin](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/universallogging)
- [Study Framework Plugin](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/unreal-study-framework)
- [Listening Test UI Plugin](https://git.rwth-aachen.de/ihta/unreal/plugins/listening-test-ui)
- [UE4 TCP Socket Plugin](https://github.com/CodeSpartan/UE4TcpSocketPlugin)

## Settings
- Control Layout for Mouse/Keyboard, HTC Vive, Valve Index, Oculus Touch and Mixed Reality


# Getting Started

Navigate into your Engine's Template folder (typically C:\\Programs\Epic Games\4.27\Templates) and open a Git Bash terminal. Enter
```
git clone --recurse-submodules https://git.rwth-aachen.de/ihta/unreal/templates/vr-listening-test-template.git
```
When creating a new project, select the category ***Architecture, Engineering and Construction*** and when everything worked out the template ***VR Listening Test Template*** should be visible.

![Select IHTA-VR-Template](/Media/Template_Selection.png)

In the next step make sure you select ***No Starter Content*** to avoid uncececcary files in your project.

![No Starter Content](/Media/No_StartContent.png)

### Starter Map
When your project is finished compiling you will see a starter map and when running the project you will be able to walk around using **W-A-S-D** and look around by holding **Right Mouse**. In the center of a map is placed a sound source which can be activated by going near it and pressing **Left Mouse**. Note that in order for sound to work, your VA server needs to be running properly. For more information visit the [Virtual Acoustics documentation](https://www.virtualacoustics.org/VA/documentation/).

![Starter Map](/Media/IhtaVrTemplate_Preview.png)
